package by.company.chesspiece;

import by.company.board.Cell;
import by.company.game.GameHolder;

public class King extends ChessPiece {

    /**
     * @return - "K" as a displaying letter for King chess piece.
     */
    @Override
    public String getDisplayingLetter() {
        return "K";
    }

    @Override
    public boolean isPossibleToMove(Cell cellToMoveFrom, Cell cellToMoveTo, Cell[][] allCells) {
        GameHolder gameHolder = new GameHolder();
        ChessPiece king = cellToMoveFrom.getCurrentChessPiece();

        /*
         * Checking 8 cells around the King chess piece, which you can move to.
         */
        try {
            if (!king.getPlayer().getId().equals(cellToMoveTo.getCurrentChessPiece().getPlayer().getId())) {
                if (cellToMoveFrom.getCoordinateX() - 1 == cellToMoveTo.getCoordinateX() &&
                        cellToMoveFrom.getCoordinateY() + 1 == cellToMoveTo.getCoordinateY()) {
                    if (gameHolder.isCheck(cellToMoveTo, cellToMoveFrom.getCurrentChessPiece().getPlayer(), allCells).size() == 0) {
                        return true;
                    }
                } else if (cellToMoveFrom.getCoordinateX() == cellToMoveTo.getCoordinateX() &&
                        cellToMoveFrom.getCoordinateY() + 1 == cellToMoveTo.getCoordinateY()) {
                    if (gameHolder.isCheck(cellToMoveTo, cellToMoveFrom.getCurrentChessPiece().getPlayer(), allCells).size() == 0) {
                        return true;
                    }
                } else if (cellToMoveFrom.getCoordinateX() + 1 == cellToMoveTo.getCoordinateX() &&
                        cellToMoveFrom.getCoordinateY() + 1 == cellToMoveTo.getCoordinateY()) {
                    if (gameHolder.isCheck(cellToMoveTo, cellToMoveFrom.getCurrentChessPiece().getPlayer(), allCells).size() == 0) {
                        return true;
                    }
                } else if (cellToMoveFrom.getCoordinateX() + 1 == cellToMoveTo.getCoordinateX() &&
                        cellToMoveFrom.getCoordinateY() == cellToMoveTo.getCoordinateY()) {
                    if (gameHolder.isCheck(cellToMoveTo, cellToMoveFrom.getCurrentChessPiece().getPlayer(), allCells).size() == 0) {
                        return true;
                    }
                } else if (cellToMoveFrom.getCoordinateX() + 1 == cellToMoveTo.getCoordinateX() &&
                        cellToMoveFrom.getCoordinateY() - 1 == cellToMoveTo.getCoordinateY()) {
                    if (gameHolder.isCheck(cellToMoveTo, cellToMoveFrom.getCurrentChessPiece().getPlayer(), allCells).size() == 0) {
                        return true;
                    }
                } else if (cellToMoveFrom.getCoordinateX() == cellToMoveTo.getCoordinateX() &&
                        cellToMoveFrom.getCoordinateY() - 1 == cellToMoveTo.getCoordinateY()) {
                    if (gameHolder.isCheck(cellToMoveTo, cellToMoveFrom.getCurrentChessPiece().getPlayer(), allCells).size() == 0) {
                        return true;
                    }
                } else if (cellToMoveFrom.getCoordinateX() - 1 == cellToMoveTo.getCoordinateX() &&
                        cellToMoveFrom.getCoordinateY() == cellToMoveTo.getCoordinateY()) {
                    if (gameHolder.isCheck(cellToMoveTo, cellToMoveFrom.getCurrentChessPiece().getPlayer(), allCells).size() == 0) {
                        return true;
                    }
                }
            }
        } catch (ArrayIndexOutOfBoundsException | NullPointerException e) {
            return false;
        }
        return false;
    }

}
