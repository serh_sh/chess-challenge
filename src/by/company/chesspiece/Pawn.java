package by.company.chesspiece;

import by.company.board.Cell;


public class Pawn extends ChessPiece {

    /**
     * @return - "P" as a displaying letter for Pawn chess piece.
     */
    @Override
    public String getDisplayingLetter() {
        return "P";
    }

    @Override
    public boolean isPossibleToMove(Cell cellToMoveFrom, Cell cellToMoveTo, Cell[][] allCells) {
        ChessPiece pawn = cellToMoveFrom.getCurrentChessPiece();
        if (pawn != null && pawn.getPlayer().getChessPieces().contains(pawn)) {
            /*
             * In this part of the algorithm we check the possibility of pawn to move straight.
             * We also check whether it is the starting player turn now, in order to find out the direction.
             */
            if (cellToMoveTo.getCoordinateX() == cellToMoveFrom.getCoordinateX()) {
                if (cellToMoveFrom.getCoordinateY() == cellToMoveTo.getCoordinateY()) {
                    return false;
                } else {
                    if (pawn.getPlayer().isStartingFirst() && cellToMoveFrom.getCoordinateY() < cellToMoveTo.getCoordinateY()) {
                        if (pawn.getStartCell().equals(cellToMoveFrom)) {
                            if (cellToMoveFrom.getCoordinateY() + 1 == cellToMoveTo.getCoordinateY()) {
                                return cellToMoveTo.getCurrentChessPiece() == null;
                            } else if (cellToMoveFrom.getCoordinateY() + 2 == cellToMoveTo.getCoordinateY()) {
                                if (allCells[cellToMoveFrom.getCoordinateY() + 1][cellToMoveFrom.getCoordinateX()].getCurrentChessPiece() == null) {
                                    return cellToMoveTo.getCurrentChessPiece() == null;
                                }
                            }
                        } else {
                            return cellToMoveFrom.getCoordinateY() + 1 == cellToMoveTo.getCoordinateY() && cellToMoveTo.getCurrentChessPiece() == null;
                        }
                    } else if (!pawn.getPlayer().isStartingFirst() && cellToMoveFrom.getCoordinateY() > cellToMoveTo.getCoordinateY()) {
                        if (pawn.getStartCell().equals(cellToMoveFrom)) {
                            if (cellToMoveFrom.getCoordinateY() - 1 == cellToMoveTo.getCoordinateY()) {
                                return cellToMoveTo.getCurrentChessPiece() == null;
                            } else if (cellToMoveFrom.getCoordinateY() - 2 == cellToMoveTo.getCoordinateY()) {
                                if (allCells[cellToMoveFrom.getCoordinateY() - 1][cellToMoveFrom.getCoordinateX()].getCurrentChessPiece() == null) {
                                    return cellToMoveTo.getCurrentChessPiece() == null;
                                }
                            }
                        } else {
                            return cellToMoveFrom.getCoordinateY() - 1 == cellToMoveTo.getCoordinateY() && cellToMoveTo.getCurrentChessPiece() == null;
                        }
                    }
                }
            } else {
                /*
                 * Here we check the ability of the pawn to pick up enemy's chess piece.
                 */
                if (pawn.getPlayer().isStartingFirst() && (cellToMoveFrom.getCoordinateX() + 1 == cellToMoveTo.getCoordinateX()
                        || cellToMoveFrom.getCoordinateX() - 1 == cellToMoveTo.getCoordinateX())
                        && cellToMoveFrom.getCoordinateY() + 1 == cellToMoveTo.getCoordinateY()
                        && cellToMoveTo.getCurrentChessPiece() != null) {
                    return true;
                } else if (!pawn.getPlayer().isStartingFirst() && (cellToMoveFrom.getCoordinateX() + 1 == cellToMoveTo.getCoordinateX()
                        || cellToMoveFrom.getCoordinateX() - 1 == cellToMoveTo.getCoordinateX())
                        && cellToMoveFrom.getCoordinateY() - 1 == cellToMoveTo.getCoordinateY()
                        && cellToMoveTo.getCurrentChessPiece() != null)
                    return true;
            }
        } else {
            System.out.println("This pawn doesn't exist or doesn't belong to you.");
            return false;
        }
        return false;
    }

}
