package by.company.chesspiece;

import by.company.board.Board;
import by.company.board.Cell;
import by.company.chesspiece.enums.CellCoordinateX;
import by.company.chesspiece.enums.CellCoordinateY;
import by.company.chesspiece.enums.ChessPieceType;
import by.company.chesspiece.enums.Colour;
import by.company.game.Player;

import java.util.Collections;

/**
 * Class for initialization of chess pieces in the cells of the board.
 */
public class ChessPieceInitializer {

    private static ChessPieceFactory chessPieceFactory = new ChessPieceFactory();

    /**
     * @param board  - Board to initialize chess pieces on.
     * @param player - For which player we should initialize chess pieces.
     */
    public static void initializeChessPiecesForPlayer(Board board, Player player) {
        Cell[][] cells = board.getCells();
        if (player.isStartingFirst()) {
            createAllChessPeaces(player, cells, CellCoordinateY.Y8.getY(), Colour.GREEN_COLOUR.getColour());
            createPawnsForCells(cells, CellCoordinateY.Y7.getY(), Colour.GREEN_COLOUR.getColour(), player);

        } else {
            createAllChessPeaces(player, cells, CellCoordinateY.Y1.getY(), Colour.RED_COLOUR.getColour());
            createPawnsForCells(cells, CellCoordinateY.Y2.getY(), Colour.RED_COLOUR.getColour(), player);
        }

    }

    private static void createAllChessPeaces(Player player, Cell[][] cells, int coordinateYForPlayer, String colour) {
        createOneChessPeaceForCells(coordinateYForPlayer, colour, cells, player, ChessPieceType.KING,
                CellCoordinateX.E.getX());
        createOneChessPeaceForCells(coordinateYForPlayer, colour, cells, player, ChessPieceType.QUEEN,
                CellCoordinateX.D.getX());
        createTwoChessPeacesForCells(coordinateYForPlayer, colour, cells, player, ChessPieceType.BISHOP,
                CellCoordinateX.C.getX(), CellCoordinateX.F.getX());
        createTwoChessPeacesForCells(coordinateYForPlayer, colour, cells, player, ChessPieceType.ROOK,
                CellCoordinateX.A.getX(), CellCoordinateX.H.getX());
        createTwoChessPeacesForCells(coordinateYForPlayer, colour, cells, player, ChessPieceType.KNIGHT,
                CellCoordinateX.B.getX(), CellCoordinateX.G.getX());
    }

    private static void createTwoChessPeacesForCells(int chessPieceCoordinateY, String colour, Cell[][] cells,
                                                     Player player, ChessPieceType chessPieceType,
                                                     int chessPieceCoordinateX, int chessPieceSecondCoordinateX) {

        ChessPiece firstChessPiece = chessPieceFactory.getChessPiece(chessPieceType);
        ChessPiece secondChessPiece = chessPieceFactory.getChessPiece(chessPieceType);

        createChessPiece(chessPieceCoordinateY, colour, cells, player, chessPieceCoordinateX, firstChessPiece);
        createChessPiece(chessPieceCoordinateY, colour, cells, player, chessPieceSecondCoordinateX, secondChessPiece);

        Collections.addAll(player.getChessPieces(), firstChessPiece, secondChessPiece);
    }

    private static void createChessPiece(int chessPieceCoordinateY, String colour, Cell[][] cells, Player player, int chessPieceSecondCoordinateX, ChessPiece secondChessPiece) {
        Cell cell = cells[chessPieceCoordinateY][chessPieceSecondCoordinateX];
        cell.setCoordinateX(chessPieceSecondCoordinateX);
        cell.setCoordinateY(chessPieceCoordinateY);
        cell.setCurrentChessPiece(secondChessPiece);
        cell.setContent(cell.changeContent(cell.getContent(), secondChessPiece.getDisplayingLetter(), colour));
        secondChessPiece.setStartCell(cell);
        secondChessPiece.setPlayer(player);
    }

    private static void createOneChessPeaceForCells(int chessPieceCoordinateY, String colour, Cell[][] cells,
                                                    Player player, ChessPieceType chessPieceType,
                                                    int chessPieceCoordinateX) {

        ChessPiece chessPiece = chessPieceFactory.getChessPiece(chessPieceType);
        createChessPiece(chessPieceCoordinateY, colour, cells, player, chessPieceCoordinateX, chessPiece);
        player.getChessPieces().add(chessPiece);
    }

    private static void createPawnsForCells(Cell[][] cells, int rowNumber, String colour, Player player) {
        for (int lineNumber = 0; lineNumber < cells[rowNumber].length; lineNumber++) {
            ChessPiece pawn = chessPieceFactory.getChessPiece(ChessPieceType.PAWN);

            Cell cell = cells[rowNumber][lineNumber];
            cell.setCoordinateX(lineNumber);
            cell.setCoordinateY(rowNumber);

            pawn.setStartCell(cell);
            pawn.setPlayer(player);
            player.getChessPieces().add(pawn);
            cell.setCurrentChessPiece(pawn);
            cell.setContent(cell.changeContent(cell.getContent(), pawn.getDisplayingLetter(), colour));
        }
    }
}
