package by.company.chesspiece;

import by.company.board.Cell;

public class Bishop extends ChessPiece {

    /**
     * @return - "B" as a displaying letter for Bishop chess piece.
     */
    @Override
    public String getDisplayingLetter() {
        return "B";
    }

    @Override
    public boolean isPossibleToMove(Cell cellToMoveFrom, Cell cellToMoveTo, Cell[][] allCells) {

        ChessPiece bishop = cellToMoveFrom.getCurrentChessPiece();

        try {
            /*
              Part of the algorithm that checks if chess piece can move to UPPER LEFT part of the board.
             */
            if (cellToMoveFrom.getCoordinateX() > cellToMoveTo.getCoordinateX() && cellToMoveFrom.getCoordinateY() > cellToMoveTo.getCoordinateY()) {
                return moveUpperLeftOrBottomRight(cellToMoveFrom, cellToMoveTo, allCells, bishop, cellToMoveTo.getCurrentChessPiece());
            }
            /*
              Part of the algorithm that checks if chess piece can move to BOTTOM RIGHT part of the board.
             */
            else if (cellToMoveFrom.getCoordinateX() < cellToMoveTo.getCoordinateX() && cellToMoveFrom.getCoordinateY() < cellToMoveTo.getCoordinateY()) {
                return moveUpperLeftOrBottomRight(cellToMoveTo, cellToMoveFrom, allCells, bishop, cellToMoveTo.getCurrentChessPiece());
            }
            /*
              Part of the algorithm that checks if chess piece can move to UPPER RIGHT part of the board.
             */
            else if (cellToMoveFrom.getCoordinateX() < cellToMoveTo.getCoordinateX() && cellToMoveFrom.getCoordinateY() > cellToMoveTo.getCoordinateY()) {
                return moveUpperRightOrBottomLeft(cellToMoveFrom, cellToMoveTo, allCells, bishop, cellToMoveTo.getCurrentChessPiece());
            }
            /*
              Part of the algorithm that checks if chess piece can move to BOTTOM LEFT part of the board.
             */
            else if (cellToMoveFrom.getCoordinateX() > cellToMoveTo.getCoordinateX() && cellToMoveFrom.getCoordinateY() < cellToMoveTo.getCoordinateY()) {
                return moveUpperRightOrBottomLeft(cellToMoveTo, cellToMoveFrom, allCells, bishop, cellToMoveTo.getCurrentChessPiece());
            }

            return false;
        } catch (ArrayIndexOutOfBoundsException e) {
            return false;
        }

    }

    private boolean moveUpperRightOrBottomLeft(Cell cellToMoveFrom, Cell cellToMoveTo, Cell[][] allCells, ChessPiece bishop, ChessPiece currentChessPiece) {
        for (int i = cellToMoveTo.getCoordinateY() + 1, j = cellToMoveTo.getCoordinateX() - 1; i < cellToMoveFrom.getCoordinateY(); i++) {
            if (allCells[i][j].getCurrentChessPiece() != null) {
                return false;
            }
            j--;
        }
        if (currentChessPiece != null && bishop.getPlayer().getId().equals(currentChessPiece.getPlayer().getId())) {
            return false;
        }
        return true;
    }

    private boolean moveUpperLeftOrBottomRight(Cell cellToMoveFrom, Cell cellToMoveTo, Cell[][] allCells, ChessPiece bishop, ChessPiece currentChessPiece) {
        for (int i = cellToMoveTo.getCoordinateY() + 1, j = cellToMoveTo.getCoordinateX() + 1; i < cellToMoveFrom.getCoordinateY(); i++) {
            if (allCells[i][j].getCurrentChessPiece() != null) {
                return false;
            }
            j++;
        }
        if (currentChessPiece != null && bishop.getPlayer().getId().equals(currentChessPiece.getPlayer().getId())) {
            return false;
        }
        return true;
    }
}
