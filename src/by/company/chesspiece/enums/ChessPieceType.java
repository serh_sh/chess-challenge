package by.company.chesspiece.enums;

/**
 * Enum for types of chess pieces.
 */
public enum ChessPieceType {
    PAWN,
    ROOK,
    KNIGHT,
    BISHOP,
    QUEEN,
    KING
}
