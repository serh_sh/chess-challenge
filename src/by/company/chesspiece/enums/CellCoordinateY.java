package by.company.chesspiece.enums;

/**
 * Enum for input strings from console for coordinate Y of the cell.
 */
public enum CellCoordinateY {
    Y1(7),
    Y2(6),
    Y3(5),
    Y4(4),
    Y5(3),
    Y6(2),
    Y7(1),
    Y8(0);
    private int y;

    CellCoordinateY(int y) {
        this.y = y;
    }

    public int getY() {
        return y;
    }
}
