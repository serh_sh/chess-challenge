package by.company.chesspiece.enums;

/**
 * Enum for input strings from console for coordinate X of the cell.
 */
public enum CellCoordinateX {
    A(0),
    B(1),
    C(2),
    D(3),
    E(4),
    F(5),
    G(6),
    H(7);

    private int x;

    CellCoordinateX(int x) {
        this.x = x;
    }

    public int getX() {
        return x;
    }
}
