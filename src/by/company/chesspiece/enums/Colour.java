package by.company.chesspiece.enums;

/**
 * Enum for colours of the content of the cell.
 */
public enum Colour {
    RED_COLOUR("\u001B[31m"),
    GREEN_COLOUR("\u001B[32m"),
    RESET_COLOUR("\u001B[0m");

    private String colour;

    public String getColour() {
        return colour;
    }

    Colour(String colour) {
        this.colour = colour;
    }
}
