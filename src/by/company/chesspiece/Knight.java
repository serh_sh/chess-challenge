package by.company.chesspiece;

import by.company.board.Cell;

public class Knight extends ChessPiece {

    /**
     * @return - "k" as a displaying letter for Knight chess piece.
     */
    @Override
    public String getDisplayingLetter() {
        return "k";
    }

    @Override
    public boolean isPossibleToMove(Cell cellToMoveFrom, Cell cellToMoveTo, Cell[][] allCells) {

        ChessPiece knight = cellToMoveFrom.getCurrentChessPiece();

        /*We reach algorithms only if the cell where chess piece wants to move
        is empty. If it is not empty, we check that it is not our chess piece.*/

        if (cellToMoveTo.getCurrentChessPiece() == null
                || knight != null && !knight.getPlayer().getId().equals(cellToMoveTo.getCurrentChessPiece().getPlayer().getId())) {

            /*
             * This part of algorithm is checking if it is possible to move knight in UPPER LEFT part of the board.
             * */

            if (cellToMoveTo.getCoordinateX() < cellToMoveFrom.getCoordinateX()
                    && cellToMoveTo.getCoordinateY() < cellToMoveFrom.getCoordinateY()) {
                if (cellToMoveTo.getCoordinateX() + 1 == cellToMoveFrom.getCoordinateX()
                        && cellToMoveTo.getCoordinateY() + 2 == cellToMoveFrom.getCoordinateY()
                        || cellToMoveTo.getCoordinateX() + 2 == cellToMoveFrom.getCoordinateX()
                        && cellToMoveTo.getCoordinateY() + 1 == cellToMoveFrom.getCoordinateY()) {
                    return true;
                }

                /*
                 * This part of algorithm is checking if it is possible to move knight in UPPER RIGHT part of the board.
                 * */

            } else if (cellToMoveTo.getCoordinateX() > cellToMoveFrom.getCoordinateX()
                    && cellToMoveTo.getCoordinateY() < cellToMoveFrom.getCoordinateY()) {
                if (cellToMoveTo.getCoordinateX() - 1 == cellToMoveFrom.getCoordinateX()
                        && cellToMoveTo.getCoordinateY() + 2 == cellToMoveFrom.getCoordinateY()
                        || cellToMoveTo.getCoordinateX() - 2 == cellToMoveFrom.getCoordinateX()
                        && cellToMoveTo.getCoordinateY() + 1 == cellToMoveFrom.getCoordinateY()) {
                    return true;
                }

                /*
                 * This part of algorithm is checking if it is possible to move knight in BOTTOM LEFT part of the board.
                 * */

            } else if (cellToMoveTo.getCoordinateX() < cellToMoveFrom.getCoordinateX()
                    && cellToMoveTo.getCoordinateY() > cellToMoveFrom.getCoordinateY()) {
                if (cellToMoveTo.getCoordinateX() + 1 == cellToMoveFrom.getCoordinateX()
                        && cellToMoveTo.getCoordinateY() - 2 == cellToMoveFrom.getCoordinateY()
                        || cellToMoveTo.getCoordinateX() + 2 == cellToMoveFrom.getCoordinateX()
                        && cellToMoveTo.getCoordinateY() - 1 == cellToMoveFrom.getCoordinateY()) {
                    return true;
                }

                /*
                 * This part of algorithm is checking if it is possible to move knight in BOTTOM RIGHT part of the board.
                 * */

            } else if (cellToMoveTo.getCoordinateX() > cellToMoveFrom.getCoordinateX()
                    && cellToMoveTo.getCoordinateY() > cellToMoveFrom.getCoordinateY()) {
                if (cellToMoveTo.getCoordinateX() - 1 == cellToMoveFrom.getCoordinateX()
                        && cellToMoveTo.getCoordinateY() - 2 == cellToMoveFrom.getCoordinateY()
                        || cellToMoveTo.getCoordinateX() - 2 == cellToMoveFrom.getCoordinateX()
                        && cellToMoveTo.getCoordinateY() - 1 == cellToMoveFrom.getCoordinateY()) {
                    return true;
                }
            }
        }
        return false;
    }
}
