package by.company.chesspiece;

import by.company.board.Cell;

public class Rook extends ChessPiece {

    /**
     * @return - "R" as a displaying letter for Rook chess piece.
     */
    @Override
    public String getDisplayingLetter() {
        return "R";
    }

    @Override
    public boolean isPossibleToMove(Cell cellToMoveFrom, Cell cellToMoveTo, Cell[][] allCells) {
        ChessPiece rook = cellToMoveFrom.getCurrentChessPiece();

        /*
         * At start we check the possibility of Rook to move UP and DOWN, that's why the X coordinates
         * of cells must be equal.
         */

        if (cellToMoveFrom.getCoordinateX() == cellToMoveTo.getCoordinateX()) {
            /*
             * This part of algorithm is checking if it is possible to move Rook to UP part of the board.
             * */
            if (cellToMoveFrom.getCoordinateY() > cellToMoveTo.getCoordinateY()) {
                return moveUpOrDown(cellToMoveFrom, cellToMoveTo, allCells, rook, cellToMoveFrom.getCoordinateX(), cellToMoveTo.getCurrentChessPiece());
            }
            /*
             * This part of algorithm is checking if it is possible to move Rook in DOWN part of the board.
             * */
            else if (cellToMoveFrom.getCoordinateY() < cellToMoveTo.getCoordinateY()) {
                return moveUpOrDown(cellToMoveTo, cellToMoveFrom, allCells, rook, cellToMoveFrom.getCoordinateX(), cellToMoveTo.getCurrentChessPiece());
            }
        }
        /*
         * Next we check the possibility of Rook to move LEFT and RIGHT, that's why the Y coordinates
         * of cells must be equal.
         */
        else if (cellToMoveFrom.getCoordinateY() == cellToMoveTo.getCoordinateY()) {
            /*
             * This part of algorithm is checking if it is possible to move Rook in LEFT part of the board.
             * */
            if (cellToMoveFrom.getCoordinateX() > cellToMoveTo.getCoordinateX()) {
                for (int i = cellToMoveFrom.getCoordinateX() - 1; i > cellToMoveTo.getCoordinateX(); i--) {
                    if (allCells[cellToMoveFrom.getCoordinateY()][i].getCurrentChessPiece() != null) {
                        return false;
                    }
                }
                if (cellToMoveTo.getCurrentChessPiece() != null && rook.getPlayer().getId().equals(cellToMoveTo.getCurrentChessPiece().getPlayer().getId())) {
                    return false;
                }
                return true;

            }
            /*
             * This part of algorithm is checking if it is possible to move Rook in RIGHT part of the board.
             * */
            else if (cellToMoveFrom.getCoordinateX() < cellToMoveTo.getCoordinateX()) {
                for (int i = cellToMoveFrom.getCoordinateX() + 1; i < cellToMoveTo.getCoordinateX(); i++) {
                    if (allCells[cellToMoveFrom.getCoordinateY()][i].getCurrentChessPiece() != null) {
                        return false;
                    }
                }
                if (cellToMoveTo.getCurrentChessPiece() != null && rook.getPlayer().getId().equals(cellToMoveTo.getCurrentChessPiece().getPlayer().getId())) {
                    return false;
                }
                return true;
            }
        } else {
            return false;
        }
        return false;
    }

    private boolean moveUpOrDown(Cell cellToMoveFrom, Cell cellToMoveTo, Cell[][] allCells, ChessPiece rook, int coordinateX, ChessPiece currentChessPiece) {
        for (int i = cellToMoveTo.getCoordinateY() + 1; i < cellToMoveFrom.getCoordinateY(); i++) {
            if (allCells[i][coordinateX].getCurrentChessPiece() != null) {
                return false;
            }
        }
        if (currentChessPiece != null && rook.getPlayer().getId().equals(currentChessPiece.getPlayer().getId())) {
            return false;
        }
        return true;
    }
}
