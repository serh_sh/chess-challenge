package by.company.chesspiece;

import by.company.board.Cell;
import by.company.chesspiece.enums.ChessPieceType;

public class Queen extends ChessPiece {

    /**
     * @return - "Q" as a displaying letter for Queen chess piece.
     */
    @Override
    public String getDisplayingLetter() {
        return "Q";
    }

    /*
     * We know that Queen chess piece moves exactly like Rook and Bishop pieces combined, that's why
     * we don't need to write own implementation method, we simply can use both implementations of Rook and Bishop chess pieces.
     */
    @Override
    public boolean isPossibleToMove(Cell cellToMoveFrom, Cell cellToMoveTo, Cell[][] allCells) {
        ChessPieceFactory chessPieceFactory = new ChessPieceFactory();
        return chessPieceFactory.getChessPiece(ChessPieceType.ROOK).isPossibleToMove(cellToMoveFrom, cellToMoveTo, allCells) ||
                chessPieceFactory.getChessPiece(ChessPieceType.BISHOP).isPossibleToMove(cellToMoveFrom, cellToMoveTo, allCells);
    }
}
