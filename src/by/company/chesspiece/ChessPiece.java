package by.company.chesspiece;

import by.company.board.Cell;
import by.company.game.GameHolder;
import by.company.game.Player;

/**
 * This class is a parent class for all other chess pieces,
 * every other chess piece class will extend this one and provide
 * their own implementation of abstract methods.
 */
public abstract class ChessPiece {

    private Cell startCell;
    private Player player;

    /**
     * This method will have different implementations depending on inheritor class.
     * In general, it is responsible for checking the possibility of a move for different chess pieces.
     *
     * @param cellToMoveFrom - Cell on the board that you want to move from.
     * @param cellToMoveTo   - Cell on the board that you want to move to.
     * @param allCells       - Two-dimensional array of all cells on the board
     * @return - Returns TRUE if it is possible to move a chess piece, FALSE - if it is not.
     */
    public abstract boolean isPossibleToMove(Cell cellToMoveFrom, Cell cellToMoveTo, Cell[][] allCells);

    /**
     * This method displays content change in Cells if it was possible to move a chess piece.
     *
     * @param cellToMoveFrom - Cell on the board that you want to move from.
     * @param cellToMoveTo   - Cell on the board that you want to move to.
     */
    public void move(Cell cellToMoveFrom, Cell cellToMoveTo) {
        cellToMoveTo.setContent(cellToMoveFrom.getContent());
        cellToMoveTo.setCurrentChessPiece(cellToMoveFrom.getCurrentChessPiece());
        cellToMoveFrom.setCurrentChessPiece(null);
        cellToMoveFrom.setContent("| |");
    }

    /**
     * Method that is used to check if it is possible to move a chess piece
     * in the way the king will be safe.
     *
     * @param cellToMoveFrom - Cell on the board that you want to move from.
     * @param cellToMoveTo   - Cell on the board that you want to move to.
     * @param allCells       - Two-dimensional array of all cells on the board
     * @param king           - Cell on the board where King chess piece is situated.
     * @return - True, if we can move chess piece to the cell and king wil be saved, False if it won't help.
     */
    public boolean tryToMove(Cell cellToMoveFrom, Cell cellToMoveTo, Cell[][] allCells, Cell king) {
        Cell tempCellToMoveFrom = new Cell();
        Cell tempCellToMoveTo = new Cell();

        tempCellToMoveFrom.setCurrentChessPiece(cellToMoveFrom.getCurrentChessPiece());
        tempCellToMoveFrom.setContent(cellToMoveFrom.getContent());
        tempCellToMoveFrom.setCoordinateY(cellToMoveFrom.getCoordinateY());
        tempCellToMoveFrom.setCoordinateX(cellToMoveFrom.getCoordinateX());

        tempCellToMoveTo.setCurrentChessPiece(cellToMoveTo.getCurrentChessPiece());
        tempCellToMoveTo.setContent(cellToMoveTo.getContent());
        tempCellToMoveTo.setCoordinateY(cellToMoveTo.getCoordinateY());
        tempCellToMoveTo.setCoordinateX(cellToMoveTo.getCoordinateX());

        cellToMoveTo.setContent(cellToMoveFrom.getContent());
        cellToMoveTo.setCurrentChessPiece(cellToMoveFrom.getCurrentChessPiece());
        cellToMoveFrom.setCurrentChessPiece(null);
        cellToMoveFrom.setContent("| |");

        GameHolder gameHolder = new GameHolder();
        if (gameHolder.isCheck(king, tempCellToMoveFrom.getCurrentChessPiece().getPlayer(), allCells).size() == 0) {
            cellToMoveTo.setContent(tempCellToMoveTo.getContent());
            cellToMoveTo.setCurrentChessPiece(tempCellToMoveTo.getCurrentChessPiece());
            cellToMoveFrom.setCurrentChessPiece(tempCellToMoveFrom.getCurrentChessPiece());
            cellToMoveFrom.setContent(tempCellToMoveFrom.getContent());
            return true;
        } else {
            cellToMoveTo.setContent(tempCellToMoveTo.getContent());
            cellToMoveTo.setCurrentChessPiece(tempCellToMoveTo.getCurrentChessPiece());
            cellToMoveFrom.setCurrentChessPiece(tempCellToMoveFrom.getCurrentChessPiece());
            cellToMoveFrom.setContent(tempCellToMoveFrom.getContent());
            return false;
        }
    }

    /**
     * @return - The displaying letter for each chess piece.
     * (For ex. P - pawn, k - knight, Q - queen etc.)
     */
    public abstract String getDisplayingLetter();

    public void setDisplayingLetter(String displayingLetter) {
    }

    public Cell getStartCell() {
        return startCell;
    }

    public void setStartCell(Cell startCell) {
        this.startCell = startCell;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
