package by.company.chesspiece;

import by.company.chesspiece.enums.ChessPieceType;

/**
 * A factory that is designed to give us an object of class that extends
 * @see ChessPiece that we ask through the
 * @see ChessPieceType enum.
 */
public class ChessPieceFactory {

    public ChessPiece getChessPiece(ChessPieceType chessPieceType) throws IllegalArgumentException {

        ChessPiece chessPiece;

        switch (chessPieceType) {
            case PAWN:
                chessPiece = new Pawn();
                break;
            case KING:
                chessPiece = new King();
                break;
            case ROOK:
                chessPiece = new Rook();
                break;
            case QUEEN:
                chessPiece = new Queen();
                break;
            case BISHOP:
                chessPiece = new Bishop();
                break;
            case KNIGHT:
                chessPiece = new Knight();
                break;
            default:
                throw new IllegalArgumentException();
        }
        return chessPiece;
    }
}
