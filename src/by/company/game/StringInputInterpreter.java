package by.company.game;

import by.company.board.Board;
import by.company.board.Cell;
import by.company.chesspiece.enums.CellCoordinateX;
import by.company.chesspiece.enums.CellCoordinateY;

/**
 * Class for interpreting keyboard input.
 */
public class StringInputInterpreter {

    public static Cell tryConvertStringToCell(String input, Board board) {
        try {
            String stringCoordinateX = String.valueOf(input.toCharArray()[0]);
            String stringCoordinateY = String.valueOf(input.toCharArray()[1]);

            int cellCoordinateX = CellCoordinateX.valueOf(stringCoordinateX.toUpperCase()).getX();
            int cellCoordinateY = CellCoordinateY.valueOf("Y".concat(stringCoordinateY)).getY();
            Cell cell = board.getCells()[cellCoordinateY][cellCoordinateX];

            return cell;
        } catch (IllegalArgumentException | ArrayIndexOutOfBoundsException e) {
            System.out.println("Wrong input format. Try again. (Some examples: A2, b1, C2)");
            return null;
        }
    }
}
