package by.company.game;

import by.company.chesspiece.ChessPiece;

import java.util.ArrayList;
import java.util.List;

/**
 * The class for players of the game.
 */
public class Player {
    private Long id;
    private List<ChessPiece> chessPieces = new ArrayList<>();
    private boolean startingFirst = false;

    public List<ChessPiece> getChessPieces() {
        return chessPieces;
    }

    public void setChessPieces(List<ChessPiece> chessPieces) {
        this.chessPieces = chessPieces;
    }

    public boolean isStartingFirst() {
        return startingFirst;
    }

    public void setStartingFirst(boolean startingFirst) {
        this.startingFirst = startingFirst;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
