package by.company.game;

import by.company.board.Board;
import by.company.board.Cell;
import by.company.chesspiece.King;

import java.util.ArrayList;
import java.util.List;

/**
 * The class that is responsible for the change of turn.
 * Contains methods for checking the ability to continue the game.
 */
public class GameHolder {
    private Player currentPlayer;
    private boolean checkmate;

    /**
     * Method for getting Cell object of King chess piece.
     *
     * @param player - Player, which king Cell object to get.
     * @param board  - Еhe board object on which the game takes place.
     * @return - Cell object of king chess piece.
     */
    public Cell getKing(Player player, Board board) {
        Cell[][] cells = board.getCells();
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                Cell cell = cells[i][j];
                if (cell.getCurrentChessPiece() instanceof King
                        && cell.getCurrentChessPiece().getPlayer().getId().equals(player.getId())) {
                    return cell;
                }
            }
        }
        return null;
    }

    /**
     * A method that checks that king can move
     * at least to one cell around.
     *
     * @param king  - Cell object of king chess piece.
     * @param board -Еhe board object on which the game takes place.
     * @return True, if king can move at least to one cell around, False if he can't.
     */
    public boolean kingCanMove(Cell king, Board board) {
        Cell[][] cells = board.getCells();
        boolean canMove;
        for (int i = king.getCoordinateX(); i < king.getCoordinateX() + 3; i++) {
            for (int j = king.getCoordinateY(); j < king.getCoordinateY() + 1; j++) {
                try {
                    canMove = king.getCurrentChessPiece().isPossibleToMove(king, cells[j - 1][i - 1], board.getCells());
                } catch (ArrayIndexOutOfBoundsException e) {
                    continue;
                }

                if (canMove) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * The method which checks whether the king
     * will be given a check if he moves to this cell.
     *
     * @param chessPieceCell - Cell where the king goes.
     * @param player - Player, which turn is it now.
     * @param cells - Two-dimensional array of all cells on the board.
     * @return - The List of Cells that can give a check to king
     */
    public List<Cell> isCheck(Cell chessPieceCell, Player player, Cell[][] cells) {
        List<Cell> enemyCells = new ArrayList<>();
        List<Cell> cellsThatThatCanBeatKing = new ArrayList<>();

        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                Cell cell = cells[i][j];
                if (cell.getCurrentChessPiece() != null && !cell.getCurrentChessPiece().getPlayer().getId().equals(player.getId())) {
                    enemyCells.add(cell);
                }
            }
        }

        for (Cell cell : enemyCells) {
            if (cell.getCurrentChessPiece() != null && cell.getCurrentChessPiece().isPossibleToMove(cell, chessPieceCell, cells)) {
                cellsThatThatCanBeatKing.add(cell);
            }
        }
        return cellsThatThatCanBeatKing;
    }

    public boolean isCheckmate() {
        return checkmate;
    }

    public void setCheckmate(boolean checkmate) {
        this.checkmate = checkmate;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public void changeCurrentPlayer(Player firstPlayer, Player secondPlayer) {
        if (firstPlayer.getId().equals(currentPlayer.getId())) {
            setCurrentPlayer(secondPlayer);
        } else {
            setCurrentPlayer(firstPlayer);
        }
    }
}
