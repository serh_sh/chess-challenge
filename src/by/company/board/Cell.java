package by.company.board;

import by.company.chesspiece.ChessPiece;
import by.company.chesspiece.enums.Colour;

/**
 * Class that is responsible for cells on the board and their's content.
 */
public class Cell {

    private String content = "| |";
    private ChessPiece currentChessPiece;
    private int coordinateX;
    private int coordinateY;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ChessPiece getCurrentChessPiece() {
        return currentChessPiece;
    }

    public void setCurrentChessPiece(ChessPiece currentChessPiece) {
        this.currentChessPiece = currentChessPiece;
    }

    public int getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(int coordinateX) {
        this.coordinateX = coordinateX;
    }

    public int getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(int coordinateY) {
        this.coordinateY = coordinateY;
    }

    /**
     * Method that provides visual changes to cells.
     * @param oldContent - Old visual content.
     * @param newContent - New visual content we want to set.
     * @param colour - Colour to paint the visual content.
     * @return - Updated content of the cell.
     */
    public String changeContent(String oldContent, String newContent, String colour) {
        return oldContent.replaceAll(" ", colour.concat(newContent).concat(Colour.RESET_COLOUR.getColour()));
    }
}
