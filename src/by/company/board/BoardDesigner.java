package by.company.board;

import by.company.chesspiece.enums.CellCoordinateX;

/**
 * This class is designed to create UI of the board
 * and initialize empty cells.
 */
public class BoardDesigner {

    public static void initializeEmptyCells(Board board) {
        Cell[][] cells = board.getCells();
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                cells[i][j] = new Cell();
                cells[i][j].setCoordinateX(j);
                cells[i][j].setCoordinateY(i);
            }
        }
    }

    public static void printBoard(Board board) {

        drawLines(board);

        for (int i = 0; i < board.getCells().length; i++) {
            for (int j = 0; j < board.getCells()[i].length; j++) {
                Cell cell = board.getCells()[i][j];
                System.out.print(cell.getContent() + "\t");
            }
            drawLines(board);
        }
        drawLetters();
    }

    private static void drawLines(Board board) {
        System.out.println();
        for (int i = 0; i < board.getCells().length * 2; i++) {
            System.out.print("+-");
        }
        System.out.println();

    }

    private static void drawLetters() {
        System.out.print(" ");
        for (CellCoordinateX x : CellCoordinateX.values()) {
            System.out.print(x.toString().concat("   "));
        }
        System.out.println();
    }

}
