package by.company.board;

public class Board {

    private static final int SQUARE_MATRIX_ORDER = 8;

    private Cell[][] cells = new Cell[SQUARE_MATRIX_ORDER][SQUARE_MATRIX_ORDER];

    public Cell[][] getCells() {
        return cells;
    }

    public void setCells(Cell[][] cells) {
        this.cells = cells;
    }
}
