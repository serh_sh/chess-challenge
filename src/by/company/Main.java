package by.company;

import by.company.board.Board;
import by.company.board.BoardDesigner;
import by.company.board.Cell;
import by.company.chesspiece.ChessPiece;
import by.company.chesspiece.ChessPieceInitializer;
import by.company.game.GameHolder;
import by.company.game.Player;
import by.company.game.StringInputInterpreter;

import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        /*
         * Initialization of all necessary objects for the game.
         */
        GameHolder gameHolder = new GameHolder();
        gameHolder.setCheckmate(false);

        Player whiteChessPlayer = new Player();
        whiteChessPlayer.setId(1L);
        whiteChessPlayer.setStartingFirst(true);
        Player blackChessPlayer = new Player();
        blackChessPlayer.setId(2L);

        gameHolder.setCurrentPlayer(whiteChessPlayer);

        Board board = new Board();


        BoardDesigner.initializeEmptyCells(board);
        ChessPieceInitializer.initializeChessPiecesForPlayer(board, whiteChessPlayer);
        ChessPieceInitializer.initializeChessPiecesForPlayer(board, blackChessPlayer);
        BoardDesigner.printBoard(board);

        /*
         * The game continues until the checkmate happens.
         */
        while (!gameHolder.isCheckmate()) {

            Cell cellToMoveFrom;
            Cell cellToMoveTo;

            /*
             * Get input from console for cell to move from.
             */
            do {
                System.out.println("Enter cell to move from: (for ex. C2)");
                Scanner input = new Scanner(System.in);
                String line = input.nextLine();
                cellToMoveFrom = StringInputInterpreter.tryConvertStringToCell(line, board);
                if (cellToMoveFrom != null && cellToMoveFrom.getCurrentChessPiece() == null) {
                    System.out.println("There are no chess pieces in current cell. Try again.");
                }
            } while (cellToMoveFrom == null || cellToMoveFrom.getCurrentChessPiece() == null);

            /*
             * Get input from console for cell to move to.
             */
            do {
                System.out.println("Enter cell to move to: (for ex. C3)");
                Scanner secondInput = new Scanner(System.in);
                String secondLine = secondInput.nextLine();
                cellToMoveTo = StringInputInterpreter.tryConvertStringToCell(secondLine, board);
            } while (cellToMoveTo == null);

            ChessPiece chessPiece = cellToMoveFrom.getCurrentChessPiece();

            /*
             * Check for what player turn is now.
             */
            if (chessPiece.getPlayer().getId().equals(gameHolder.getCurrentPlayer().getId())) {
                Cell king = gameHolder.getKing(gameHolder.getCurrentPlayer(), board);
                /*
                 * Get List of Cells that can gave a check to king
                 */
                List<Cell> checkCells = gameHolder.isCheck(king, gameHolder.getCurrentPlayer().getId()
                        .equals(whiteChessPlayer.getId()) ? whiteChessPlayer : blackChessPlayer, board.getCells());
                int checksCount = checkCells.size();

                /*
                 * If it is possible to move a chess piece
                 */
                if (chessPiece.isPossibleToMove(cellToMoveFrom, cellToMoveTo, board.getCells())) {
                    /*
                     * If there are no chess piece available to give a king check, we can move.
                     */
                    while (checksCount != 0) {
                        if (checksCount == 1) {
                            if (!gameHolder.kingCanMove(king, board)) {
                                if (chessPiece.tryToMove(cellToMoveFrom, cellToMoveTo, board.getCells(), king)) {
                                    checksCount = 0;
                                } else {
                                    System.out.println("You can't move that way.");
                                    break;
                                }
                            }
                        } else if (checksCount == 2 && !gameHolder.kingCanMove(king, board)) {
                            System.out.println("CHECKMATE");
                            gameHolder.setCheckmate(true);
                        }
                    }
                    if (!gameHolder.isCheckmate() && checksCount == 0) {
                        if (chessPiece.tryToMove(cellToMoveFrom, cellToMoveTo, board.getCells(), king)) {
                            chessPiece.move(cellToMoveFrom, cellToMoveTo);
                            BoardDesigner.printBoard(board);
                            gameHolder.changeCurrentPlayer(blackChessPlayer, whiteChessPlayer);
                        } else {
                            System.out.println("You can't move that way.");
                        }
                    } else {
                        System.out.println("You can't move that way.");
                    }
                } else {
                    System.out.println("You can't move that way.");
                }
            } else {
                System.out.println("It's not your turn to play.");
            }
        }
    }
}
